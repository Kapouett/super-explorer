// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FilesystemManager {
	private FSDir root;

	private Crawler crawler;

	private WatchService watchService;

	private static Logger log = LogManager.getLogger("File system");

	public FilesystemManager() {
		try {
			watchService = FileSystems.getDefault().newWatchService();
		} catch(Exception e) {
			log.error("Error creating file watch service", e);
		}

		log.info("Filesystem root: "+System.getProperty("user.dir"));
		
		root = new FSDir("", null, Paths.get("."));
		loadDir(root);

		crawler = new Crawler(root);
		crawler.start();
	}

	public FSDir root() { return root; }

	/**
	 * Attempts to find a node from a path
	 * @param root
	 * @param splitPath
	 * @return Node if found, null otherwhise
	 */
	public FSNode get(FSDir root, String... splitPath) { // TODO: Test
		FSNode res = null;
		FSDir curDir = root;

		// Ensure the root is loaded
		loadDir(curDir);

		if(splitPath.length == 0) {
			return curDir;
		}

		for(int i=1; i<splitPath.length-1; i++) {
			curDir = curDir.dirs.get(splitPath[i]);

			if(curDir == null) return null;

			if(curDir.loaded) curDir.refresh();
			else loadDir(curDir);
		}
		res = curDir.getNode(splitPath[splitPath.length-1]);
		if(res instanceof FSDir)
			loadDir((FSDir)res);

		return res;
	}

	/**
	 * Recursive case-insensitive search for full match in file names
	 * @param root
	 * @param query
	 * @return Unsorted list of files
	 */
	public List<FSFile> searchFiles(FSDir root, String query) {
		// Ensure the root is loaded
		loadDir(root);

		query = query.toLowerCase();

		List<FSFile> res = new LinkedList<>();

		List<FSDir> dirsToSearch = new LinkedList<>();
		dirsToSearch.add(root);

		while( !dirsToSearch.isEmpty() ) {
			FSDir current = dirsToSearch.get(0);
			dirsToSearch.remove(0);
			current.refresh();
			for( FSFile f : current.files.values() ) {
				if( f.name.toLowerCase().contains(query) )
					res.add(f);
			}
			// Add subdirs
			for( FSDir dir : current.dirs.values() ) {
				if(loadDir(dir))
					dirsToSearch.add(dir);
			}
		}

		return res;
	}

	/**
	 * Load content of a dir (non-recursive). Calling this on an already loaded dir will have no effect
	 * @param dir
	 * @return is the directory loaded
	 */
	public boolean loadDir(FSDir dir) {
		if(dir.loaded) return true;

		synchronized(this) {
			// Test this again in case the directory got loaded while we were waiting for sync
			if(dir.loaded) return true;

			Path p = Paths.get(".", dir.getPath());

			try( DirectoryStream<Path> stream = Files.newDirectoryStream(p) ) {
				Iterator<Path> it = stream.iterator();
				while( it.hasNext() )
					dir.registerElem( it.next().getFileName().toString() );

					dir.loaded = true;
			} catch(Exception e) {
				e.printStackTrace();
				dir.loaded = true;
			}
			return dir.loaded;
		}
	}

	private String getMime(Path p) {
		String mimeType = "";
		try {
			mimeType = Files.probeContentType(p);
		} catch (IOException e) {
			log.error("Error getting mime for "+p, e);
		}
		if(mimeType == null || mimeType.isEmpty()) return "application/octet-stream";
		return mimeType;
	}


	
	/**
	 * Zip a directory's first level files (contained directories are left empty)
	 * @param dir
	 * @return Path to the zip, or null if failed
	 */
	public Path zipDir(FSDir dir) {
		Path res = null;
		log.info("Zipping dir \""+dir.getPath()+"\"");
		try {
			res = Files.createTempFile("super-explorer-"+dir.getPath().hashCode(), ".zip");
			File file = res.toFile();
			file.deleteOnExit();
			
			try( ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(file)) ) {
				for (String nodeName : dir.contentNames()) {
					FSNode node = dir.getNode(nodeName);
					if( node instanceof FSDir ) {
						zos.putNextEntry(new ZipEntry(nodeName+"/"));
						zos.closeEntry();
					} else if( node instanceof FSFile ) {
						zos.putNextEntry(new ZipEntry(nodeName));
						Files.copy(node.asFile().toPath(), zos);
						zos.closeEntry();
					}
				}
				
				zos.close();
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return res;
	}


	/* 
	 * File system representation
	 */

	public abstract class FSNode implements Comparable<FSNode> {
		public final String name;
		public final FSDir parent;

		private FSNode(String name, FSDir parent) { this.name = name; this.parent = parent; }

		public String getPath() {
			if(parent == null) return "";
			return parent.getPath()+'/'+name;
		}

		public File asFile() {
			return Paths.get(".", getPath()).toFile();
		}

		@Override
		public int compareTo(FSNode o) {
			int res = name.compareTo(o.name);
			if( res == 0 ) {
				if(parent == null)
					return -1;
				if(o.parent == null)
					return 1;
				res = parent.compareTo(o.parent);
			}
			return res;
		}
	}

	public class FSDir extends FSNode {
		private final Set<String> content = new HashSet<>();
		private final Map<String, FSDir> dirs = new HashMap<>();
		private final Map<String, FSFile> files = new HashMap<>();
		private Optional<FSFile> thumbnail = Optional.empty();

		private WatchKey key;

		private boolean loaded = false;

		private FSDir(String name, FSDir parent, Path path) {
			super(name, parent);
			try {
				key = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
			} catch (IOException e) {
				log.error("Couldn't register dir to watch service", e);
			}
		}

		public int countFiles() { 
			refresh();
			return files.size();
			}

		public int countDirs() {
			refresh();
			return dirs.size();
		}

		public Set<String> contentNames() {
			refresh();
			return Collections.unmodifiableSet(content);
		}

		public Set<String> fileNames() {
			refresh();
			return Collections.unmodifiableSet(files.keySet());
		}

		public Collection<FSFile> getFiles() {
			refresh();
			return Collections.unmodifiableCollection(files.values());
		}

		public FSNode getNode(String name) {
			FSNode dir = dirs.get(name);
			if(dir == null)
				return files.get(name);
			return dir;
		}

		public FSDir getDir(String name) { return dirs.get(name); }

		public FSFile getFile(String name) { return files.get(name); }

		/**
		 * @return an image or video from this folder if present
		 */
		public Optional<FSFile> getThumbnailFile() {
			return thumbnail;
		}
		
		/**
		 * Check if current thumbnail is still valid. 
		 * If not, iterate over folder's files to find a new one.
		 */
		private void updateThumbnail() {
			if( thumbnail.isPresent() ) {
				String currThumb = thumbnail.get().getPath();
				Path currThumbPath = Paths.get(".", currThumb);
				String mimeType = getMime(currThumbPath);
				if( Files.isRegularFile(currThumbPath) && mimeType.startsWith("video/") || mimeType.startsWith("image/") )
					return;
			}
			
			thumbnail = Optional.empty();
			
			for(FSFile file : files.values()) {
				String mimeType = getMime( Paths.get(".", file.getPath()) );
				if( mimeType.startsWith("video/") || mimeType.startsWith("image/") )
					thumbnail = Optional.of(file);
			}
		}

		public void refresh() {
			if(!loaded) return;
			
			for( WatchEvent<?> event : key.pollEvents() ) {
				@SuppressWarnings("rawtypes")
				WatchEvent.Kind kind = event.kind();
				@SuppressWarnings("unchecked")
				Path name = ((WatchEvent<Path>)event).context();

				if(kind == StandardWatchEventKinds.ENTRY_CREATE) {
					log.info("Detected file creation: "+getPath()+"/"+name);
					registerElem(name.toString());
				} else if(kind == StandardWatchEventKinds.ENTRY_DELETE) {
					log.info("Detected file deletion: "+getPath()+"/"+name);
					unregisterElem(name.toString());
				}
			}
		}
		
		/**
		 * Register a new child of this directory
		 * @param name
		 */
		private void registerElem(String name) {
			Path path = Paths.get( ".", getPath(), name );
			
			if( !Files.isReadable(path) ) {
				log.warn("Read access denied on: "+path.toString());
				return;
			}
			
			if( Files.isRegularFile(path) ) {
				String mimeType = getMime(path);
				FSFile fsFile = new FSFile(name, this, mimeType);
				files.put(name, fsFile);
				content.add(name);
				
				if( !thumbnail.isPresent() && mimeType.startsWith("video/") || mimeType.startsWith("image/") )
					thumbnail = Optional.of(fsFile);
			} else if( Files.isDirectory(path) ) {
				dirs.put(name, new FSDir(name, this, path));
				content.add(name);
			} else {
				log.warn("This is neither a file nor a dir: "+path.toString());
			}
		}
		
		/**
		 * Unregister a child from this dir. 
		 * Use this when the child has been removed. 
		 * If the child is a directory, this will unregister it from the watch service. 
		 * If the child is a file, this will update the thumbnail
		 * @param name
		 */
		private void unregisterElem(String name) {
			FSNode childNode = null;
			if( Files.isDirectory( Paths.get(".", getPath(), name) ) ) {
				childNode = dirs.get(name);
				dirs.remove(name);
				if(childNode != null) ((FSDir)childNode).unload();
			} else {
				childNode = files.get(name);
				files.remove(name);
				
				// If this file was the thumbnail, remove it
				if( !thumbnail.isPresent() && (FSFile)childNode == thumbnail.get() ) {
					thumbnail = Optional.empty();
					updateThumbnail();
				}
			}
			content.remove(name);
		}
		
		/**
		 * Unload this directory and its sub directories
		 */
		private void unload() {
			log.info("Unloading "+getPath());
			key.cancel();
			for( FSDir entry : dirs.values() )
				entry.unload();
			dirs.clear();
			files.clear();
			content.clear();
		}
	}

	public class FSFile extends FSNode {
		public final String mime;

		private FSFile(String name, FSDir parent, String mime) {
			super(name, parent);
			this.mime = mime;
		}
	}



	/* 
	 * Background loading
	 */

	private static int nextCrawler = 0;
	private static Semaphore crawlerIdSem = new Semaphore(1);
	/**
	 * Thread running a breadth-first exploration and caching of the given directory
	 */
	private class Crawler extends Thread {
		FSDir root;

		private Crawler(FSDir root) {
			super("Crawler-"+nextCrawler);
			this.root = root;
			try {
				crawlerIdSem.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			nextCrawler++;
			crawlerIdSem.release();
		}

		@Override
		public void run() {
			String rootDisplayName = root.name;
			if(rootDisplayName.isEmpty())
				rootDisplayName = "/";
			log.info("Starting a file system crawler in "+rootDisplayName+"...");

			loadDir(root);

			List<FSDir> dirsToLoad = new LinkedList<>();
			dirsToLoad.add(root);

			while( !dirsToLoad.isEmpty() ) {
				FSDir current = dirsToLoad.get(0);
				dirsToLoad.remove(0);

				// Add subdirs
				for( FSDir dir : current.dirs.values() ) {
					if(loadDir(dir))
						dirsToLoad.add(dir);
				}
			}

			log.info("File system crawler for \""+rootDisplayName+"\" ended");
		}
	}
}
