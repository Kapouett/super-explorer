// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import superexplorer.httpserver.HTTPServer;
import superexplorer.httpserver.ResponseProvider;

public class Main {

	private Main() {}
	
	private static Logger log = LogManager.getLogger("Main");
	
	private static int threadCount = 64;

	public static void main(String[] args) throws Exception {
		int port = 8000;
		String address = "localhost";

		for( int i = 0; i<args.length; i++ ) {
			if( i == 0 )
				address = args[i];
			else if( i == 1 ) {
				try {
					port = Integer.parseInt(args[i]);
				} catch(NumberFormatException e) {
					log.fatal( "Argument 2 is not a number, should be port", e );
					System.exit(1);
				}
			}
		}

		@SuppressWarnings("resource")
		ServerSocket server = new ServerSocket(port, 10, InetAddress.getByName(address));

		log.info("Super Explorer started on "+address+":"+port);
		
		log.info("Connection thread pool size: "+threadCount);
		
		ResponseProvider provider = new SuperExplorer(new FilesystemManager());
		
		ExecutorService executor = Executors.newFixedThreadPool(threadCount, new NamedThreadFactory("Connection"));
		
		/**
		 * loop to keep the application alive - a new HTTPServer object is
		 * created for each client
		 */
		while(true) {
			try {
				Socket connected = server.accept();
				HTTPServer httpServer = new HTTPServer(connected, provider);
				executor.execute(httpServer);
			} catch(Exception e) {
				log.error(e);
			}
		}
	}
	
	private static class NamedThreadFactory implements ThreadFactory {
		private final String name;
		private long nextId = 0;
		
		private NamedThreadFactory(String name) {
			this.name = name;
		}
		
		@Override
		public Thread newThread(Runnable r) {
			return new Thread(r, name+"-"+(nextId++));
		}
	}

}
