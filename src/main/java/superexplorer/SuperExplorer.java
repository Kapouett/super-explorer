// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import superexplorer.FilesystemManager.FSDir;
import superexplorer.FilesystemManager.FSFile;
import superexplorer.FilesystemManager.FSNode;
import superexplorer.httpserver.ReqHeaderField;
import superexplorer.httpserver.RespHeaderField;
import superexplorer.httpserver.ResponseStatus;
import superexplorer.httpserver.Response;
import superexplorer.httpserver.ResponseProvider;

public class SuperExplorer implements ResponseProvider {
	private static final String mediaResPrefix = "!SuperExplorer/media/";
	private static final int mediaResPrefixLen = mediaResPrefix.length();
	
	private final FilesystemManager fs;
	
	private static Logger log = LogManager.getLogger("SuperExplorer");
	
	public SuperExplorer(FilesystemManager fs) {
		this.fs = fs;
	}

	@Override
	public void processGet(String query, Response res, Map<ReqHeaderField, String> reqHeader) {
		// Handle range request
		boolean rangeReq = false;
		long rangeLow = -1, rangeHi = -1;
		String rangeHeader = reqHeader.get(ReqHeaderField.Range);
		if( rangeHeader != null ) { // Range header is present
			String[] rangeHeaderParts = rangeHeader.split("=");

			if(rangeHeaderParts.length == 2 && rangeHeaderParts[0].equalsIgnoreCase("bytes")) { // Range field is valid
				String[] rangeParts = rangeHeaderParts[1].split("-"); // Separate range
				if(rangeParts.length == 2) { // Min and max provided
					try {
						rangeLow = Long.parseLong( rangeParts[0] );
						rangeHi = Long.parseLong( rangeParts[1] );

						if( rangeHi > rangeLow )
							rangeReq = true;
					} catch(Exception e) {
						rangeReq = false;
					}
				} else if( rangeParts.length == 1 ) { // Only min value is provided, assuming max as file end
					try {
						rangeLow = Long.parseLong( rangeParts[0] );
						rangeReq = true;
					} catch(Exception e) {
						rangeReq = false;
					}
				}
			}
		}

		String path = query;

		Map<String, String> getArgs = new HashMap<>();
		// Separate and URL-decode GET arguments
		int qm = path.indexOf('?');
		if( qm >= 0 ) {			
			String[] argsArray = path.substring(qm+1).split("&");
			for(int i=0; i<argsArray.length; i++) {
				String argClean;
				
				try {
					argClean = URLDecoder.decode(argsArray[i], "UTF-8");
				} catch (UnsupportedEncodingException e) {
					log.error(e);
					argClean = argsArray[i];
				}
				
				int eqIndex = argClean.indexOf('=');
				if(eqIndex < 0)
					getArgs.put(argClean, "");
				else
					getArgs.put(argClean.substring(0, eqIndex), argClean.substring(eqIndex+1));
			}

			path = path.substring(0, qm);
		}
		
		// URL-decode path
		try {
			path = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error(e);
			error(res, ResponseStatus.HTTP_500);
			return;
		}
		
		// Resources
		if( path.startsWith('/'+mediaResPrefix) ) {
			sendResource(res, path.substring(mediaResPrefixLen+1));
			return;
		}

		String[] splitPath = path.split("/");

		FSNode node = fs.get(fs.root(), splitPath);
		
		if( node == null ) {
			error(res, ResponseStatus.HTTP_404);
		} else if( !authorizedPath(splitPath) ) {
			error(res, ResponseStatus.HTTP_403);
		} else if( node instanceof FSFile ) {
			if(rangeReq)
				sendFile(res, (FSFile)node, rangeLow, rangeHi);
			else
				sendFile(res, (FSFile)node);
		} else if( node instanceof FSDir ) {
			if( getArgs.containsKey("view") ) // Fileview
				fileView(res, (FSDir)node, splitPath, getArgs);
			else if( getArgs.containsKey("s") ) // Search results
				searchResults(res, (FSDir)node, splitPath, getArgs.get("s"));
			else if( getArgs.containsKey("download") ) // Download directory
				zipDir( res, (FSDir)node );
			else // Normal folder view
				folder(res, (FSDir)node, splitPath);
		} else {
			error(res, ResponseStatus.HTTP_500);
		}
	}
	
	@Override
	public void processHead(String query, Response res, Map<ReqHeaderField, String> reqHeader) {
		error(res, ResponseStatus.HTTP_501);
	}
	
	@Override
	public void processPost(String query, Response res, Map<ReqHeaderField, String> reqHeader, BufferedReader reqBody) {
		error(res, ResponseStatus.HTTP_501);
	}

	private static boolean authorizedPath(String[] splitPath) {
		for(int i = 0; i<splitPath.length; i++) {
			if( splitPath[i].equals("..") )
				return false;
		}

		return true;
	}

	private void sendFile(Response res, FSFile file) {
		File jFile = file.asFile();
		long fileLen = jFile.length();
		try( FileInputStream in = new FileInputStream(jFile) ) {
			setBasicHeader(res, ResponseStatus.HTTP_200);
			res.setHeader(RespHeaderField.ContentLength, String.valueOf(fileLen));
			res.sendHeader();
			
			res.addBytes(in, -1);
		} catch (Exception e) {
			log.error(e);
			error(res, ResponseStatus.HTTP_500);
		}
	}
	
	private void sendFile(Response res, FSFile file, long min, long max) {
		File jFile = file.asFile();
		long fileLen = jFile.length();
		try( FileInputStream in = new FileInputStream(jFile) ) {
			if(max <= 0)
				max = fileLen;
			else
				max = Math.min(max, fileLen);
			
			long length = max-min;
			
			setBasicHeader(res, ResponseStatus.HTTP_206);
			res.setHeader(RespHeaderField.ContentRange, min+"-"+max+"/"+fileLen);
			res.setHeader(RespHeaderField.ContentLength, String.valueOf(length));
			res.sendHeader();
			
			in.skip(min);

			res.addBytes(in, length);
			
			log.debug("Sending bytes "+min+"-"+max);
		} catch (IOException e) {
			log.error(e);
			error(res, ResponseStatus.HTTP_500);
		}
	}
	
	private void sendResource(Response res, String path) {
		log.debug("Resource request: "+path);

		try( InputStream in = SuperExplorer.class.getClassLoader().getResourceAsStream(path) ) {
			if(in == null) {
				error(res, ResponseStatus.HTTP_404);
				return;
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			long fileLen = 0;
			int val = in.read();
			while( val != -1 ) {
				fileLen++;
				out.write(val);
				val = in.read();
			}
			
			setBasicHeader(res, ResponseStatus.HTTP_200);
			res.setHeader(RespHeaderField.ContentLength, String.valueOf(fileLen));
			res.setHeader(RespHeaderField.CacheControl, "max-age=1000");

			String pathLC = path.toLowerCase();
			String mime = "application/octet-stream";
			if(pathLC.endsWith(".js"))
				mime = "text/javascript";
			else if(pathLC.endsWith(".css"))
				mime = "text/css";
			res.setHeader(RespHeaderField.ContentType, mime);
			
			res.sendHeader();
			res.addBytes(out);
		} catch (Exception e) {
			log.error(e);
			error(res, ResponseStatus.HTTP_500);
		}
	}

	private void folder(Response res, FSDir dir, String[] path) {
		String name = "/", parentPath = "";		
		if(!dir.name.isEmpty())
			name = dir.name;
		if(dir.parent != null)
			parentPath = encodePath(dir.parent.getPath());
		if(parentPath.isEmpty())
			parentPath = "/";
		
		setBasicHeader(res, ResponseStatus.HTTP_200);
		res.setHeader(RespHeaderField.ContentType, "text/html");
		res.sendHeader();

		res.addString(htmlBeginHeader("&#128193; "+name));
		res.addString(styleSheetRes("style_dir.css"));
		res.addString("<script src=\"/"+mediaResPrefix+"script_dir.js\"></script>");
		res.addString("</head><body>");
		
		// Thumbnail
		res.addString("<img id=\"thumbnail_img\" class=\"thumbnail\" src=\"\" alt=\"\" />\n" + 
				"<video id=\"thumbnail_vid\" class=\"thumbnail\" src=\"\" alt=\"\" preload=\"metadata\" muted autoplay ></video>");

		// Download button
		res.addString("<h1><a id=\"downloadButton\" title=\"Zip and download\" download=\""+name+".zip\" href=\"?download\">&#x1F4E5;</a>&nbsp;");

		// Title
		res.addString("<a title=\"Root\" href=\"/\">&#128193;</a>&nbsp;<span style=\"font-size: 70%\">");
		String navPath = "";
		for(int i=0; i<path.length-1; i++) {
			navPath += encodePath(path[i])+"/";
			res.addString("<a href=\""+navPath+"\">"+path[i]+"</a>/");
		}
		res.addString("</span>"+name);
		
		// Search bar
		res.addString(" <form class=\"search_form\"><input type=\"text\" name=\"s\" placeholder=\"Search\" /></form></h1>");

		// Parent folder button
		res.addString("<a href=\""+ parentPath +"\" class=\"nav\">↑&nbsp;Previous</a>");

		String folders = "", files = "";
		
		Set<String> nodesSet = dir.contentNames();
		String[] nodesNames = nodesSet.toArray(new String[nodesSet.size()]);
		Arrays.parallelSort(nodesNames);
		
		int numFiles = 0, numFolders = 0;
		for( String nodeName : nodesNames ) {
			FSNode node = dir.getNode(nodeName);
			if( node instanceof FSFile ) { // File
				FSFile nodeF = (FSFile) node;
				files += "<li id=\"f_"+numFiles+"\"";
				if( nodeF.mime.startsWith("image/") )
					files += "onmouseover=\"showThumbnailImg(&quot;"+encodePath(nodeF.getPath())+"&quot;)\"";
				else if( nodeF.mime.startsWith("video/") )
					files += "onmouseover=\"showThumbnailVid(&quot;"+encodePath(nodeF.getPath())+"&quot;)\"";
				files += " onmouseleave=\"hideThumbnail()\"><a href=\"?view="+String.valueOf(numFiles)+"\">&#128196; "+ node.name +"</a></li>";
				numFiles++;
			} else { // Directory
				folders += "<li id=\"d_"+numFolders+"\"";
				
				fs.loadDir((FSDir)node);
				Optional<FSFile> thumbnailFile = ((FSDir)node).getThumbnailFile();
				boolean hasThumb = thumbnailFile.isPresent();
				if( hasThumb ) {
					if( thumbnailFile.get().mime.startsWith("image/") )
						folders += " onmouseover=\"showThumbnailImg('";
					else if( thumbnailFile.get().mime.startsWith("video/") )
						folders += " onmouseover=\"showThumbnailVid('";
					else
						hasThumb = false;
				}
				if(hasThumb) 
					folders += encodePath(thumbnailFile.get().getPath())+"')\" onmouseleave=\"hideThumbnail()\"";
				
				folders += "><a href=\""+ encodePath(node.getPath()) +"\">&#128193; "+ node.name +"</a></li>";
				numFolders++;
			}
		}
		
		res.addString("<h2>"+numFolders+" folders&nbsp;:</h2><ul class=\"folders\">");
		res.addString(folders);
		res.addString("</ul><h2>"+numFiles+" files&nbsp;:</h2><ul class=\"files\">");
		res.addString(files);
		res.addString("</ul>");

		res.addString("</body></html>");
	}
	
	private void searchResults(Response res, FSDir root, String[] rootPath, String query) {
		List<FSFile> result = fs.searchFiles( root, query );
		Collections.sort(result);
		String rootPathStr = root.getPath();
		if(rootPathStr.isEmpty())
			rootPathStr = "/";
		
		setBasicHeader(res, ResponseStatus.HTTP_200);
		res.setHeader(RespHeaderField.ContentType, "text/html");
		res.sendHeader();
		
		res.addString(htmlBeginHeader("&#128270; "+query));
		res.addString(styleSheetRes("style_dir.css"));
		res.addString("<script src=\"/"+mediaResPrefix+"script_dir.js\"></script>");
		res.addString("</head><body>");
		
		// Thumbnail
		res.addString("<img id=\"thumbnail_img\" class=\"thumbnail\" src=\"\" alt=\"\" />\n" + 
				"<video id=\"thumbnail_vid\" class=\"thumbnail\" src=\"\" alt=\"\" preload=\"metadata\" muted autoplay ></video>");

		res.addString("<h1>Results for ");
		// Search bar
		res.addString(" <form class=\"search_form\"><input type=\"text\" name=\"s\" value=\"");
		res.addString(query);
		res.addString("\" /></form> in "+rootPathStr+"</h1>");
		
		// Parent folder button
		res.addString("<a href=\"?\" class=\"nav\">↑&nbsp;Previous</a>");
		
		res.addString("</ul><h2>"+result.size()+" files&nbsp;:</h2><ul class=\"files\">");
		int numFiles = 0;
		for(FSFile f : result) {
			res.addString( "<li id=\"f_"+numFiles+"\"" );
			
			if( f.mime.startsWith("image/") )
				res.addString( "onmouseover=\"showThumbnailImg(&quot;"+encodePath(f.getPath())+"&quot;)\"" );
			else if( f.mime.startsWith("video/") )
				res.addString( "onmouseover=\"showThumbnailVid(&quot;"+encodePath(f.getPath())+"&quot;)\"" );
			res.addString(" onmouseleave=\"hideThumbnail()\"");
			
			res.addString( " title=\""+ f.getPath().replace("\"", "&quot;") +"\"" );
			
			res.addString( "><a href=\""+encodePath(root.getPath()) +"?s="+query+"&view="+numFiles+
					"\">&#128196; "+ f.name +"</a></li>" );
			numFiles++;
		}
		res.addString("</ul>");
		
		res.addString("</body></html>");
	}
	
	private void zipDir(Response res, FSDir dir) {
		Path zip = fs.zipDir(dir);
		
		if( zip == null || !zip.toFile().isFile() ) {
			res.setStatus(ResponseStatus.HTTP_404);
		} else {
			setBasicHeader(res, ResponseStatus.HTTP_200);
			
			File zipFile = zip.toFile();
			try( FileInputStream in = new FileInputStream(zipFile) ) {
				res.setHeader(RespHeaderField.ContentLength, String.valueOf(zipFile.length()));
				res.setHeader(RespHeaderField.ContentType, "application/zip");
				String fileName = dir.name;
				if(fileName.isEmpty())
					fileName = "root";
				res.setHeader(RespHeaderField.ContentDisposition, "filename=\""+fileName+".zip\"");
				res.sendHeader();
				
				res.addBytes(in, zipFile.length());
				
			} catch (FileNotFoundException e) {
				res.setStatus(ResponseStatus.HTTP_500);
				res.setHeader(RespHeaderField.ContentLength, "0");
				e.printStackTrace();
			} catch (IOException e) {
				res.setStatus(ResponseStatus.HTTP_500);
				res.setHeader(RespHeaderField.ContentLength, "0");
				e.printStackTrace();
			}
			
			
		}
	}

	private void fileView(Response res, FSDir dir, String[] path, Map<String, String> args) {
		int idSelf;
		try {
			idSelf = Integer.valueOf(args.get("view"));
		} catch(Exception e) { // File id is not a number
			error(res, ResponseStatus.HTTP_400);
			return;
		}
		
		/** Folder containing this file */
		String parentPath = dir.getPath();
		if(parentPath.isEmpty())
			parentPath = "/";
		
		/** Search query */
		String searchQuery = "";
		boolean isSearch = false;
		if(args.containsKey("s")) {
			isSearch = true;
			searchQuery = args.get("s");
		}
		
		FileViewerData viewerData = new FileViewerData(dir, idSelf, isSearch, searchQuery);
		
		if(!viewerData.isValid()) {
			error(res, ResponseStatus.HTTP_404);
			return;
		}

		FSFile prevFile = viewerData.getPrev();
		FSFile nextFile = viewerData.getNext();
		FSFile file = viewerData.getCurrent();
		String filePathEncoded = encodePath(file.getPath());

		// Copy args and generate a new args string (without view)
		Map<String, String> argsTmp = new HashMap<>(args);
		argsTmp.remove("view");
		// Create get arguments for next and previous files
		argsTmp.put("view", String.valueOf(viewerData.idPrev));
		String argsPrev = makeGetArgsString(argsTmp);
		argsTmp.put("view", String.valueOf(viewerData.idNext));
		String argsNext = makeGetArgsString(argsTmp);

		// Response headers
		setBasicHeader(res, ResponseStatus.HTTP_200);
		res.setHeader(RespHeaderField.ContentType, "text/html");
		res.sendHeader();
		
		res.addString(htmlBeginHeader("&#128196; "+file.name));
		res.addString(styleSheetRes("style_view.css"));
		res.addString("<script src=\"/"+mediaResPrefix+"script_view.js\"></script>");
		
		res.addString("</head><body>");

		// Page container
		res.addString("<div id=\"outer\">");
		
		res.addString("<nav class=\"nav\">");
		
		// Download button
		res.addString("<span class=\"title\" style=\"position: absolute; left: 20px; top:0; z-index: 2;\">");
		res.addString("<h1><a id=\"downloadButton\" title=\"Download\" download=\""+file.name+"\" href=\""+filePathEncoded+"\">&#x1F4E5;</a>&nbsp;");
		
		// File name
		res.addString(file.name+"</h1></span>");
		
		// Nav bar
		res.addString("<span style=\"z-index: 1;\">");
		// Back to folder
		res.addString("<a class=\"nav\" style=\"margin-right: 20px;\" href=\"");
		if(viewerData.isSearch) {
			res.addString("?s="+encode(viewerData.searchQuery));
		} else {
			res.addString("?");
		}
		res.addString("#f_"+idSelf+"\">&uarr;&nbsp;Back</a>");
		
		// Prev file
		res.addString( "<a id=\"nav_prev\" class=\"nav\" rel=\"prev\" href=\"" );
		res.addString( argsPrev+"\" title=\""+prevFile.name );
		res.addString( "\">&larr;&nbsp;Prev</a>" );
		
		// File counter
		res.addString("<span class=\"nav\"><span id=\"fileId\">"+(viewerData.idSelf+1)+"</span>/<span id=\"fileCount\">"+viewerData.count+"</span></span>");
		
		// Next file
		res.addString( "<a id=\"nav_next\" class=\"nav\" rel=\"next\" href=\"" );
		res.addString( argsNext+"\" title=\""+nextFile.name );
		res.addString( "\">Next&nbsp;&rarr;</a>" );
		
		res.addString("</span></nav>");
		
		// Media container
		res.addString("<div id=\"media-container\" data-mime=\""+file.mime+
				"\" data-url=\""+filePathEncoded.replaceAll("\"", "\\\"")+"\"></div>");


		// Close page container
		res.addString("</div>");
		res.addString("</body></html>");
	}
	
	@Override
	public void error(Response res, ResponseStatus status) {
		res.clearHeader();
		setBasicHeader(res, status);
		res.setHeader(RespHeaderField.ContentType, "text/html");
		res.sendHeader();
		res.addString(htmlBeginHeader(String.valueOf(status.code))+styleSheetRes("style_dir.css")+"</head><body>");
		res.addString("<img src=\"https://http.cat/"+status.code+"\" alt=\""+status.code);
		res.addString("\" class=\"error_pic\" /></body></html>");
	}
	
	/**
	 * Set status, accept-ranges and server fields of the header
	 * @param res
	 * @param status
	 * @return
	 */
	private static Response setBasicHeader(Response res, ResponseStatus status) {
		res.setStatus(status);
		res.setHeader(RespHeaderField.AcceptRanges, "bytes");
		res.setHeader(RespHeaderField.Server, "Super Server");
		return res;
	}
	
	/**
	 * Apply URL encoding to a string
	 * @param str
	 * @return
	 */
	private static String encode(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error(e);
			return str;
		}
	}
	
	/**
	 * Apply URL encoding on a path, keeping forward slashes
	 * @param path
	 * @return
	 */
	private static String encodePath(String path) {
		return encode(path).replace("%2F", "/");
	}
	
	/**
	 * Create a GET arguments string from a list of args. The string starts with '?'. Arguments are URL encoded.
	 * @param args
	 * @return
	 */
	public static String makeGetArgsString(Map<String, String> args) {
		if(args.isEmpty()) return "";
		
		String res = "?";
		boolean first = true;
		for(Entry<String, String> arg : args.entrySet()) {
			String argStr = encode(arg.getKey());
			if(!arg.getValue().isEmpty())
				argStr += '='+encode(arg.getValue());
			
			if(first) {
				first = false;
				res += argStr;
			} else
				res += '&'+argStr;
		}
		return res;
	}
	
	private static String htmlBeginHeader(String title) {
		return "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>"+title+"</title>";
	}
	
	private static String styleSheetRes(String path) {
		return "<link rel=\"stylesheet\" type=\"text/css\" href=\"/"+mediaResPrefix+path+"\">";
	}
	
	private class FileViewerData {
		final FSDir dir;
		final int count;
		final int idSelf;
		final int idPrev;
		final int idNext;
		final boolean isSearch;
		final String searchQuery;
		private List<FSFile> files = new ArrayList<>();
		
		private boolean isValid = false;
		
		FileViewerData(FSDir dir, int id, boolean isSearch, String searchQuery) {
			this.dir = dir;
			this.idSelf = id;
			this.isSearch = isSearch;
			this.searchQuery = searchQuery;
			
			// Find files in the folder
			if(isSearch)
				files.addAll(fs.searchFiles(dir, searchQuery));
			else
				files.addAll(dir.getFiles());
			count = files.size();
			
			// Invalid file id
			if(idSelf < 0 || idSelf >= files.size()) {
				isValid = false;
				
				idPrev = 0;
				idNext = 0;
				return;
			}
			
			Collections.sort(files);
			int idPrevT = idSelf - 1;
			if(idPrevT < 0) idPrevT = count-1;
			idPrev = idPrevT;
			idNext = (idSelf + 1) % count;
			
			isValid = true;
		}
		
		public boolean isValid() {
			return isValid;
		}
		
		public List<FSFile> getFiles() {
			return Collections.unmodifiableList(files);
		}
		
		public FSFile getCurrent() {
			return files.get(idSelf);
		}
		
		public FSFile getPrev() {
			return files.get(idPrev);
		}
		
		public FSFile getNext() {
			return files.get(idNext);
		}
	}
}
