// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer.httpserver;

public enum ResponseStatus {
	/** 200 OK */
	HTTP_200(200, "200 OK"),
	/** 206 Partial Content */
	HTTP_206(206, "206 Partial Content"),
	/** 400 Bad Request */
	HTTP_400(400, "400 Bad Request"),
	/** 403 Forbidden */
	HTTP_403(403, "403 Forbidden"),
	/** 404 Not Found */
	HTTP_404(404, "404 Not Found"),
	/** 416 Range Not Satisfiable */
	HTTP_416(416, "416 Range Not Satisfiable"),
	/** 500 Internal Server Error */
	HTTP_500(500, "500 Internal Server Error"),
	/** 501 Not Implemented */
	HTTP_501(501, "501 Not Implemented"),
	/** 505 HTTP Version Not Supported */
	HTTP_505(505, "505 HTTP Version Not Supported");
	
	public final int code;
	public final String status;
	
	private ResponseStatus( int code, String status ) {
		this.code = code;
		this.status = status;
	}
}
