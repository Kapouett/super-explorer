// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer.httpserver;

public enum ReqHeaderField {
	Accept("Accept"),
	AcceptCharset("Accept-Charset"),
	AcceptEncoding("Accept-Encoding"),
	AcceptLanguage("Accept-Language"),
	Connection("Connection"),
	Host("Host"),
	Range("Range"),
	Referer("Referer"),
	UserAgent("User-Agent");
	
	public final String string;
	
	private ReqHeaderField(String string) {
		this.string = string;
	}
}
