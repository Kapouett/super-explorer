// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer.httpserver;

import java.io.BufferedReader;
import java.util.Map;

public interface ResponseProvider {
	/**
	 * Respond to a GET request
	 * @param query
	 * @param res
	 * @param reqHeader
	 */
	public void processGet(String query, Response res, Map<ReqHeaderField, String> reqHeader);
	
	public void processHead(String query, Response res, Map<ReqHeaderField, String> reqHeader);
	
	public void processPost(String query, Response res, Map<ReqHeaderField, String> reqHeader, BufferedReader reqBody);
	
	/**
	 * Fill the response with an error
	 * @param res
	 * @param status error code
	 */
	public void error(Response res, ResponseStatus status);
}
