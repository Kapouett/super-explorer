// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer.httpserver;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Response {
	private boolean hasBody;
	private DataOutputStream out;
	private ByteArrayOutputStream tempOut = new ByteArrayOutputStream();
	private Map<RespHeaderField, String> header = new HashMap<>();
	private ResponseStatus status = ResponseStatus.HTTP_500;
	
	private static Logger log = LogManager.getLogger("Response");
	
	private static final int BUFFER_SIZE = 100000;
	
	private boolean headerSent = false;
	
	Response(DataOutputStream out, boolean hasBody) {
		this.out = out;
		this.hasBody = hasBody;
	}
	
	public Response setStatus(ResponseStatus status) {
		if(headerSent) {
			errHeaderSent();
			return this;
		}
		if(status == null) {
			log.error("Status code cannot be null!");
			return this;
		}
		this.status = status;
		return this;
	}
	
	public Optional<ResponseStatus> getStatus() {
		if(status == null) return Optional.empty();
		return Optional.of(status);
	}
	
	public Response setHeader(RespHeaderField field, String value) {
		if(headerSent) {
			errHeaderSent();
			return this;
		}
		header.put(field, value);
		return this;
	}
	
	public Response clearHeader() {
		if(headerSent) {
			errHeaderSent();
			return this;
		}
		header.clear();
		return this;
	}
	
	public Response sendHeader() {
		if(headerSent) {
			errHeaderSent();
			return this;
		}
		
		if( !getStatus().isPresent() ) {
			log.error("Cannot send header: no http status set");
			return this;
		}
		
		String NEW_LINE = "\r\n";
		String headerStr = "";
		
		try {
			headerStr += "HTTP/1.1 " + status.status + NEW_LINE;
			for(Entry<RespHeaderField, String> e : header.entrySet()) {
				headerStr += e.getKey().string + e.getValue() + NEW_LINE;
			}
			
			// Finish header with an empty line
			out.writeBytes( headerStr + NEW_LINE );
			
			headerSent = true;
			
			// Empty tempOut into out
			tempOut.flush();
			tempOut.writeTo(out);
			
		} catch (SocketException e) {
			log.warn("Couldn't send header, socket closed");
		} catch (IOException e) {
			log.warn("Couldn't send header", e);
		}
		
		return this;
	}
	
	public boolean headersSent() { return headerSent; }
	
	
	public Response addBytes(byte[] bytes) {
		if(!hasBody) {
			errNoBody();
			return this;
		}
		
		try {
			if( headerSent )
				out.write(bytes);
			else
				tempOut.write(bytes);
		} catch (SocketException e) {
			log.warn("Couldn't send bytes, socket closed");
		} catch (IOException e) {
			log.warn("Couldn't send bytes", e);
		}
		return this;
	}

	/**
	 * Add bytes from an input stream
	 * @param in
	 * @param count number of bytes to send (negative value to send all available bytes)
	 */
	public Response addBytes(InputStream in, long count) {
		if(!hasBody) {
			errNoBody();
			return this;
		}
		
		try {
			byte[] bytes;
			int sizeRead;
			if( count >= 0 ) {
				bytes = new byte[(int) Math.min(count, BUFFER_SIZE)];
				sizeRead = in.read(bytes);
				
				while( sizeRead > 0 && count > 0 ) {
					if( headerSent )
						out.write(bytes, 0, sizeRead);
					else
						tempOut.write(bytes, 0, sizeRead);
					sizeRead = in.read(bytes);
					count-=sizeRead;
				}
			} else {
				bytes = new byte[BUFFER_SIZE];
				sizeRead = in.read(bytes);
				
				while( sizeRead > 0 ) {
					if( headerSent )
						out.write(bytes, 0, sizeRead);
					else
						tempOut.write(bytes, 0, sizeRead);
					sizeRead = in.read(bytes);
				}
			}
		} catch (SocketException e) {
			log.warn("Couldn't send bytes, socket closed");
		} catch (IOException e) {
			log.warn("Couldn't send bytes", e);
		}
		return this;
	}
	
	public Response addBytes(ByteArrayOutputStream in) {
		if(!hasBody) {
			errNoBody();
			return this;
		}
		
		try {
			if( headerSent )
				in.writeTo(out);
			else
				in.writeTo(tempOut);
		} catch (IOException e) {
			log.warn("Couldn't send bytes", e);
		}
		return this;
	}
	
	public Response addString(String str) {
		if(!hasBody) {
			errNoBody();
			return this;
		}
		
		try {
			if( headerSent )
				out.write(str.getBytes(StandardCharsets.UTF_8));
			else
				tempOut.write(str.getBytes(StandardCharsets.UTF_8));
		} catch (SocketException e) {
			log.warn("Couldn't send bytes, socket closed");
		} catch (IOException e) {
			log.warn("Couldn't send bytes", e);
		}
		return this;
	}
	
	private void errHeaderSent() {
		log.error("Header has already been sent!");
	}
	
	private void errNoBody() {
		log.error("This response does not have a body!");
	}

}
