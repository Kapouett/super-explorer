// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer.httpserver;

public enum RespHeaderField {
	AcceptRanges("Accept-Ranges:"),
	CacheControl("Cache-Control:"),
	Connection("Connection:"),
	ContentLength("Content-Length:"),
	ContentRange("Content-Range:"),
	ContentType("Content-Type:"),
	ContentDisposition("Content-Disposition:"),
	Server("Server:");
	
	public final String string;
	
	private RespHeaderField(String string) {
		this.string = string;
	}
}
