// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

package superexplorer.httpserver;

import static superexplorer.httpserver.ResponseStatus.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HTTPServer implements Runnable {
	private static final Map<String, ReqHeaderField> reqHeaderFields;

	static {
		reqHeaderFields = new HashMap<>();
		for(ReqHeaderField field : ReqHeaderField.values())
			reqHeaderFields.put(field.string.toLowerCase(), field);
	}
	
	private Logger log = LogManager.getLogger("HTTP Server");
	
	private Map<ReqHeaderField, String> reqHeader = new HashMap<>();

	private Socket client = null;
	private BufferedReader inClient = null;
	private DataOutputStream outClient = null;
	
	private ResponseProvider provider;

	public HTTPServer(Socket cl, ResponseProvider provider) {
		client = cl;
		this.provider = provider;
	}

	@Override
	public void run() {
		String logStr = "Connection ended";
		try {
			log.info("Connection from " + client.getInetAddress() + ":" + client.getPort());

			inClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
			outClient = new DataOutputStream(client.getOutputStream());
			
			String requestString = inClient.readLine();
			String headerLine = requestString;

			// sometimes headerLine is null
			if (headerLine == null) {
				log.error("Header line is null, ending connection");
				return;
			}

			StringTokenizer tokenizer = new StringTokenizer(headerLine);
			String httpMethod = tokenizer.nextToken();
			String httpQueryString = tokenizer.nextToken();

			if( !log.isDebugEnabled() )
				log.info("Request: "+requestString);
			String printRequest = "Request:";
			while (inClient.ready()) { // TODO: solve infinite loop on POST requests
				/** Read the HTTP request until the end */
				if( log.isDebugEnabled() )
					printRequest += "\n |"+requestString;

				String[] split = requestString.split(":", 2);
				if( split.length == 2 ) {
					ReqHeaderField key = reqHeaderFields.get( split[0].trim().toLowerCase() );
					if( key != null ) {
						reqHeader.put( key, split[1].trim() );
					} else {
						log.debug("Unsupported header field: "+requestString);
					}
				}

				requestString = inClient.readLine();
			}
			log.debug(printRequest);

			Response res;
			
			switch(httpMethod) {
			case "GET":
				res = new Response(outClient, true);
				provider.processGet(httpQueryString, res, Collections.unmodifiableMap(reqHeader));
				break;
			case "HEAD":
				res = new Response(outClient, false);
				provider.processHead(httpQueryString, res, Collections.unmodifiableMap(reqHeader));
				break;
			case "POST":
				res = new Response(outClient, true);
				provider.processPost(httpQueryString, res, Collections.unmodifiableMap(reqHeader), inClient);
				break;
			default:
				res = new Response(outClient, true);
				log.error("Unknown http method: "+httpMethod);
				provider.error(res, HTTP_501);
			}
			
			if( !res.headersSent() ) res.sendHeader();
			outClient.flush();
			
			if(res.getStatus().isPresent())
				logStr = "Responded with "+res.getStatus().get().code;
			else
				log.warn("No response code");
		} catch (Exception e) {
			log.error(e);
		} finally {
			try {
				outClient.close();
			} catch (IOException e) {
				log.error(e);
			}
		}
		log.info(logStr+", "+outClient.size()+" bytes sent");
	}
}
