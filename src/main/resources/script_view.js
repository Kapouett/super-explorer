// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

window.onload = loadMedia;
document.onkeydown = checkKey;

function checkKey(e) {
  e = e || window.event;

  if (e.keyCode == '37') { //Left arrow
    window.open(document.getElementById("nav_prev").getAttribute("href"), "_self");
  } else if (e.keyCode == '39') { //Right arrow
    window.open(document.getElementById("nav_next").getAttribute("href"), "_self");
  }
}

function loadMedia() {
	var container = document.getElementById("media-container");

	// Clear container
	while(container.firstChild) {
		container.removeChild(container.firstChild);
	}

	var mime = container.getAttribute("data-mime");
	var mediaUrl = container.getAttribute("data-url");
	var mediaName = mediaUrl;
	var i = mediaName.lastIndexOf("/");
	if( i >= 0 )
		mediaName = mediaName.substring(i);

	var child;

	if( mime.startsWith("image/") ) { // Image
		child = document.createElement("a");
		child.setAttribute("href", document.getElementById("nav_next").getAttribute("href"));
		var img = document.createElement("img");
		img.setAttribute("src", mediaUrl);
		img.setAttribute("alt", mediaName);
		img.setAttribute("class", "media");
		child.appendChild(img);

	} else if( mime.startsWith("video/") ) { // Video
		child = document.createElement("video");
		child.setAttribute("src", mediaUrl);
		child.setAttribute("class", "media");
		child.setAttribute("controls", "controls");
		child.setAttribute("loop", "loop");
		child.setAttribute("autoplay", "autoplay");
		child.innerText = mediaName;

	} else if( mime.startsWith("audio/") ) { // Audio
		child = document.createElement("audio");
		child.setAttribute("src", mediaUrl);
		child.setAttribute("class", "media");
		child.setAttribute("controls", "controls");
		child.setAttribute("loop", "loop");
		child.setAttribute("autoplay", "autoplay");
		child.setAttribute("style", "width: 90%;");
		child.innerText = mediaName;

	} else if( mime === "text/html" ) { // HTML
		child = document.createElement("iframe");
		child.setAttribute("src", mediaUrl);
		child.setAttribute("class", "media");
		child.setAttribute("style", "border:none; width:100%; height:100%;");

	} else if( mime.startsWith("text/") || mime === "application/json" || mime === "application/x-php" ) { // Plain text, JSON, PHP
		child = rawText(mediaUrl);

	} else if(mime === "application/pdf") { // PDF
		child = document.createElement("iframe");
		child.setAttribute("src", mediaUrl);
		child.setAttribute("class", "media");
		child.setAttribute("style", "border:none; width:100%; height:100%;");

	} else if( mime.includes("flash") ) { // Flash
		child = document.createElement("embed");
		child.setAttribute("src", mediaUrl);
		child.setAttribute("class", "media");
		child.setAttribute("type", mime);
		child.setAttribute("style", "border:none; width:100%; height:100%;");

	} else { // Other
		child = document.createElement("p");
		child.innerText = "Unknown format: "+mime;
	}

	container.appendChild(child);
}

function rawText(url) {
	var pre = document.createElement("pre");
	pre.setAttribute("class", "media");
	pre.setAttribute("style", "width: 100%; height: 100%;");
	var lineList = document.createElement("ol");
	lineList.setAttribute("class", "codelines");
	pre.appendChild(lineList);
			

	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", url, true);
	rawFile.onreadystatechange = function() {
		if(rawFile.readyState === 4) {
			if(rawFile.status === 200 || rawFile.status === 0) {
				var lines = rawFile.responseText.split('\n');
				lines.forEach(function(line) {
					if( line ) {
						var lineElem = document.createElement("code");
						lineElem.innerText = line;
					} else {
						var lineElem = document.createElement("br");
					}
					var listElem = document.createElement("li");
					listElem.setAttribute("class", "codeline");
					
					listElem.appendChild(lineElem);
					lineList.appendChild(listElem);
				} );
			}
		}
	}
	rawFile.send(null);
	return pre;
}

function next() {
	var nextId = findParameter( document.getElementById("nav_next").getAttribute("href"), "s");
	changeFile( nextId, window.location.href, findGetParameter("s") );
}

function changeFile( newId, newPath, newSearch ) {
	if( newSearch == null )
		var jsonData = { "id":newId, "location":newPath }
	else
		var jsonData = { "id":newId, "location":newPath, "search":newSearch }

	var req = new XMLHttpRequest();
	req.open("POST", window.location.href);
	req.setRequestHeader("Content-Type","application/json;charset=UTF-8");
	req.send(JSON.stringify(jsonData));
	req.onreadystatechange = function() {
		if(req.readyState === 4) {
			if(req.status === 200 || req.status === 0) {
				var resData = JSON.parse(req.responseText);

				document.getElementById("fileId").innerText = resData.idSelf+1;
				document.getElementById("fileCount").innerText = resData.fileCount;

				document.getElementById("media-container").setAttribute("data-mime", resData.mime);
				document.getElementById("media-container").setAttribute("data-url", resData.url);

				loadMedia();
			}
		}
	}
}

function findGetParameter(parameterName) {
	var source = window.location.href;
	var i = source.indexOf("?");
	if( i>0 )
		source = source.substring(i+1);
	return findParameter(source, parameterName);
}

function findParameter(source, parameterName) {
	var result = null, tmp = [];
	source.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

