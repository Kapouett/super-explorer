// SPDX-License-Identifier: MIT
// Copyright © 2021 Kapouett

function showThumbnailImg(url) {
	document.getElementById("thumbnail_img").src = url;
	document.getElementById("thumbnail_img").style.display = 'block';
}

function showThumbnailVid(url) {
	document.getElementById("thumbnail_vid").src = url+'#t=10';
	document.getElementById("thumbnail_vid").style.display = 'block';
}

function hideThumbnail() {
	document.getElementById("thumbnail_img").style.display = 'none';
	document.getElementById("thumbnail_img").src = '';
	document.getElementById("thumbnail_vid").style.display = 'none';
	document.getElementById("thumbnail_vid").src = '';
}

