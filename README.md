# Super Explorer

Simple (and probably terrible) java server allowing working directory to be browsed over http.

Default address and port are localhost:8000, but you can pass custom values as arguments

Supported formats for the viewer (based on mime types):
- Image
- Video
- Audio
- HTML
- Plain text
- Json
- PHP
- PDF
- Flash

## Building
Clone and run `./gradlew build` (or `gradlew.bat build` on windows) to produce a runnable jar in build/libs
